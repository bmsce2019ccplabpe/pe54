
 #include<stdio.h>
int input()
{
 int n;
 printf("enter a number\n");
 scanf("%d",&n);
 return n;
}
void input_array(int n,int a[n])
{
 int i;
 for(i=0;i<n;i++)
  {
   printf("enter an element\n");
   scanf("%d",&a[i]);
  }
 }
 void compute(int n,int a[n],int *lpos,int *spos)
 { 
  int large= a[0];
  int small= a[0];
  *lpos = 0;
  *spos= 0;
  
  for(int i=1;i<n;i++)
  {
   if(large<a[i])
    { 
     large=a[i];
     *lpos=i;
    }
    if(small>a[i])
    {
     small=a[i];
     *spos=i; 
    }
  }
  int temp=a[*lpos];
  a[*lpos]=a[*spos];
  a[*spos]=temp;
 }
 void output(int n,int a[n],int lpos,int spos)
 {
  int i;
  for(i=0;i<n;i++)
  {
   printf("%d",a[i]);
  }
  printf("\n position of largest number is %d & smallest number is %d\n",lpos,spos);
 }
 int main()
 {
  int n= input();
  int a[n];
  input_array(n,a);
  int lpos,spos;
  compute(n,a,&lpos,&spos);
  output(n,a,lpos,spos);
 }
